﻿using System;

namespace test_build
{
    class Program
    {
        public static void Main(string[] args)
        {
            HelloWorld();
            AddSecondFunction();
            AnotherMethod();
            LastTest();
        }

        public static void HelloWorld()
        {
            Console.WriteLine("Hello World!");
        }

        public static void AddSecondFunction()
        {
            Console.WriteLine("Second hello World");
        }

        public static void AnotherMethod()
        {
            Console.WriteLine("Third hi World");
        }

        public static void LastTest()
        {
            Console.WriteLine("Last test");
        }
    }
}